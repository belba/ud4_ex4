package ud4_ex4;

public class ex4 {

	public static void main(String[] args) {
		
		/*4 - Escribe un programa Java que declare una variable entera N y asígnale un valor. A
		continuación escribe las instrucciones que realicen los siguientes:*/		
		
		int n = 1;
		
		int resultSuma = n+77;
		int resultResta = resultSuma-3;
		
		System.out.println("Valor inicial de N ="+ n);
		System.out.println("N + 77 "+ resultSuma);
		System.out.println("N - 3 "+ resultResta);
		System.out.println("N * 2 "+ resultResta*2);
		
	}

}
